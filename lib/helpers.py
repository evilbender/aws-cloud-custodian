import boto3
import json
from pprint import pprint
from six import itervalues


REGION_SHORTS = {
    'us-east-1': 'US East (N. Virginia)',
    'us-east-2': 'US East (Ohio)',
    'us-west-1': 'US West (N. California)',
    'us-west-2': 'US West (Oregon)',
    'ca-central-1': 'Canada (Central)',
    'eu-west-1': 'EU (Ireland)',
    'eu-central-1': 'EU (Frankfurt)',
    'eu-west-2': 'EU (London)',
    'ap-northeast-1': 'Asia Pacific (Tokyo)',
    'ap-northeast-2': 'Asia Pacific (Seoul)',
    'ap-southeast-1': 'Asia Pacific (Singapore)',
    'ap-southeast-2': 'Asia Pacific (Sydney)',
    'ap-south-1': 'Asia Pacific (Mumbai)',
    'sa-east-1': 'South America (Sao Paulo)'
}

SERVICECODES = {
    'ebs': 'AmazonEC2',
    'rds': 'AmazonRDS',
    'ec2': 'AmazonEC2'
}


class Pricing:
    def __init__(self):
        self.client = boto3.client('pricing')

    def _fieldgen(self, field, value):
        return {
            'Type': 'TERM_MATCH',
            'Field': field,
            'Value': value
        }

    def _productFamily(self, servicecode):
        pager = self.client.get_paginator('get_products')
        page_iter = pager.paginate(ServiceCode=servicecode,
                                   Filters=[self._fieldgen('location', REGION_SHORTS['us-east-1'])])
        r = [[json.loads(y) for y in x['PriceList']] for x in page_iter]
        rr = []
        [[rr.append(y) for y in x] for x in r]
        return set([x['product']['productFamily'] for x in rr])

    @property
    def product_types(self):
        pager = self.client.get_paginator('describe_services')
        page_iter = pager.paginate()
        r = [x['Services'] for x in page_iter]
        z = []
        for x in r:
            z += x
        return set([x['ServiceCode'] for x in z])

    def product_attributes(self, servicecode):
        r = self.client.describe_services(ServiceCode=servicecode)
        return r['Services'][0]['AttributeNames']

    def get_price(self, c7nobj):
        _filters = [self._fieldgen('location', REGION_SHORTS[c7nobj['item_region']])]

        def ebs_snapshot():
            _filters.append(self._fieldgen('ServiceCode', 'AmazonEC2'))
            _filters.append(self._fieldgen('productFamily', 'Storage Snapshot'))
            _filters.append(self._fieldgen('usagetype', 'EBS:SnapshotUsage'))

        def ebs():
            _filters.append(self._fieldgen('ServiceCode', 'AmazonEC2'))
            _filters.append(self._fieldgen('productFamily', 'Storage'))
            _filters.append(self._fieldgen('volumeType', 'General Purpose'))

        def ec2():
            _filters.append(self._fieldgen('ServiceCode', 'AmazonEC2'))
            _filters.append(self._fieldgen('preInstalledSw', 'NA'))
            _filters.append(self._fieldgen('capacitystatus', 'Used'))
            _filters.append(self._fieldgen('instanceType', c7nobj['InstanceType']))
            _filters.append(self._fieldgen('operatingSystem', 'Linux'))

        itemtype = c7nobj['item_type']

        if itemtype == 'ec2':
            # todo: make this look up ebs value for powered off ec2 instances
            ec2()
            servicecode = 'AmazonEC2'
            c7nobj.update({'this_unit': 1})
            unit = 'this_unit'
        elif itemtype == 'ebs':
            ebs()
            servicecode = 'AmazonEC2'
            unit = 'Size'
        elif itemtype == 'ebs-snapshot':
            ebs_snapshot()
            servicecode = 'AmazonEC2'
            unit = 'VolumeSize'
        elif itemtype == 'rds':
            raise NotImplemented

        # pprint(_filters)
        r = self.client.get_products(ServiceCode=servicecode, Filters=_filters)['PriceList']
        r = [json.loads(x) for x in r]
        if len(r) != 1:
            return r
            raise LookupError("Found {} items in pricelist".format(len(r)))
        r = r[0]['terms']['OnDemand']
        try:
            return float(next(itervalues(next(itervalues(r))['priceDimensions']))['pricePerUnit']['USD']) * float(c7nobj[unit])
        except KeyError as e:
            pprint(e)
            return r

    def _price_request(self, _servicecode, _filters):
        pager = self.client.get_paginator()
        page_iter = pager.paginate(ServiceCode=_servicecode, Filters=_filters)
        return [x for x in page_iter]

    def get_aws_obj(self, c7nobj):
        itemtype = c7nobj['item_type']
        itemregion = c7nobj['item_region']
        if itemtype == 'ec2':
            c = boto3.client('ec2', region_name=itemregion)
            r = c.describe_instances(InstanceIds=[c7nobj['InstanceId']])['Reservations'][0]['Instances']
            if len(r) != 1:
                raise LookupError("Found {} ec2 instances".format(len(r)))
            return r[0]
        elif itemtype == 'ebs':
            c = boto3.client('ec2', region_name=itemregion)
            r = c.describe_volumes(VolumeIds=[c7nobj['VolumeId']])['Volumes']
            if len(r) != 1:
                raise LookupError("Found {} Volumes".format(len(r)))
            else:
                return r[0]
        elif itemtype == 'rds':
            c = boto3.client('rds')
            r = c.describe_db_instances(DBInstanceIdentifier=c7nobj['DBInstanceIdentifier'])['DBInstances']
            if len(r) != 1:
                raise LookupError("Found {} DB Instances".format(len(r)))
            return r[0]
        elif itemtype == 'ebs-snapshot':
            c = boto3.client('ec2', region_name=itemregion)
            r = c.describe_snapshots(SnapshotIds=[c7nobj['SnapshotId']])
            return r['Snapshots'][0]