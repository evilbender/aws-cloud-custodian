import boto3
import requests
import json
import os
import glob
from pprint import pprint
from six import itervalues
from lib.helpers import Pricing


REGION_SHORTS = {
    'us-east-1': 'US East (N. Virginia)',
    'us-east-2': 'US East (Ohio)',
    'us-west-1': 'US West (N. California)',
    'us-west-2': 'US West (Oregon)',
    'ca-central-1': 'Canada (Central)',
    'eu-west-1': 'EU (Ireland)',
    'eu-central-1': 'EU (Frankfurt)',
    'eu-west-2': 'EU (London)',
    'ap-northeast-1': 'Asia Pacific (Tokyo)',
    'ap-northeast-2': 'Asia Pacific (Seoul)',
    'ap-southeast-1': 'Asia Pacific (Singapore)',
    'ap-southeast-2': 'Asia Pacific (Sydney)',
    'ap-south-1': 'Asia Pacific (Mumbai)',
    'sa-east-1': 'South America (Sao Paulo)'
}


def get_ebs_pricing(storageType=None, product='AmazonEC2'):
    _filters = [
        {
            'Type': 'TERM_MATCH',
            'Field': 'productFamily',
            'Value': 'Storage'
        },
        {
            'Type': 'TERM_MATCH',
            'Field': 'location',
            'Value': 'US East (N. Virginia)'
        }
    ]
    if storageType in ['gp2']:
        storageType = 'General Purpose'
        _filters.append({
            'Type': 'TERM_MATCH',
            'Field': 'volumeType',
            'Value': storageType
        })
    pricing = boto3.client('pricing')
    pager = pricing.get_paginator('get_products')
    page_iter = pager.paginate(ServiceCode=product, Filters=_filters)
    r = []
    for x in page_iter:
        for y in x.get('PriceList'):
            r.append(json.loads(y))
    if storageType and len(r) == 1:
        r = list(r[0]['terms']['OnDemand'].values())[0]['priceDimensions']
        r = list(r.values())[0]['pricePerUnit']['USD']
        return float(r)
    elif storageType and len(r) > 1:
        raise LookupError
    elif storageType is None and r:
        return r
    else:
        raise LookupError


def get_rds_pricing(DBInstanceClass=None, Engine=None, c7nobj=None):
    pricing = boto3.client('pricing')
    _filters = [
        {
            'Type': 'TERM_MATCH',
            'Field': 'location',
            'Value': 'US East (N. Virginia)'
        }
    ]
    if c7nobj:
        engine_types = {
            'mysql': 'MySQL',
            'oracle': 'Oracle',
            'aurora-mysql': 'Aurora MySQL',
            'aurora': 'PostgreSQL',
            'sqlserver-ex': 'SQL Server',
            'mariadb': 'MariaDB'
        }
        # DB Engines: ['MariaDB', 'PostgreSQL', 'Oracle', 'Aurora MySQL', 'SQL Server', 'MySQL']
        # c7n Engines: ['aurora', 'mariadb', 'mysql', 'aurora-mysql', 'sqlserver-ex']
        if c7nobj['MultiAZ']:
            deploymentOption = 'Multi-AZ'
        else:
            deploymentOption = 'Single-AZ'

        _filters.append(
            {
                'Type': 'TERM_MATCH',
                'Field': 'deploymentOption',
                'Value': deploymentOption
            }
        )

        _filters.append(
            {
                'Type': 'TERM_MATCH',
                'Field': 'databaseEngine',
                'Value': engine_types[c7nobj['Engine'].lower()]
            }
        )
        _filters.append(
            {
                'Type': 'TERM_MATCH',
                'Field': 'instanceType',
                'Value': c7nobj['DBInstanceClass']
            }
        )

        if c7nobj.get('LicenseModel') and c7nobj.get('Engine') == 'sqlserver-ex':
            lic = {
                'license-included': 'License included',
                'bring-your-own': 'Bring your own license'
            }
            _filters.append(
                {
                    'Type':  'TERM_MATCH',
                    'Field': 'licenseModel',
                    'Value': lic[c7nobj['LicenseModel'].lower()]
                }
            )
        if c7nobj.get('Engine') == 'sqlserver-ex':
            _filters.append(
                {
                    'Type': 'TERM_MATCH',
                    'Field': 'databaseEdition',
                    'Value': 'Express'
                }
            )

    if Engine:
        _filters.append(
            {
                'Type': 'TERM_MATCH',
                'Field': 'databaseEngine',
                'Value': Engine
            })
    if DBInstanceClass:
        _filters.append(
            {
                'Type': 'TERM_MATCH',
                'Field': 'instanceType',
                'Value': DBInstanceClass
            }
        )
    pager = pricing.get_paginator('get_products')
    # pprint(_filters)
    page_iter = pager.paginate(ServiceCode='AmazonRDS', Filters=_filters)
    r = []
    # return [x for x in page_iter]
    for x in page_iter:
        for y in x.get('PriceList'):
            r.append(json.loads(y))
    if c7nobj and len(r) != 1:
        raise LookupError("Found multiple RDS type matches.")
    elif c7nobj and len(r) == 1:
        return float(next(itervalues(next(itervalues(r[0]['terms']['OnDemand']))['priceDimensions']))['pricePerUnit']['USD'])
    return r


def get_pricing(product, instanceType):
    _filters = [
        {
            'Type': 'TERM_MATCH',
            'Field': 'instanceType',
            'Value': instanceType
        },
        {
            'Type': 'TERM_MATCH',
            'Field': 'preInstalledSw',
            'Value': 'NA'
        },
        {
            'Type': 'TERM_MATCH',
            'Field': 'location',
            'Value': 'US East (N. Virginia)'
        },
        {
            'Type': 'TERM_MATCH',
            'Field': 'operatingSystem',
            'Value': 'Linux'
        },
        {
            'Type': 'TERM_MATCH',
            'Field': 'capacitystatus',
            'Value': 'Used'
        }
    ]
    pricing = boto3.client('pricing')
    pager = pricing.get_paginator('get_products')
    page_iter = pager.paginate(ServiceCode=product, Filters=_filters)
    r = []
    # return [x for x in page_iter]
    for x in page_iter:
        for y in x.get('PriceList'):
            r.append(json.loads(y))
    return r


def get_ebs_volumes(vols, region):
    if type(vols) is not list:
        raise ValueError
    c = boto3.client('ec2', region_name=region)
    return c.describe_volumes(VolumeIds=vols)['Volumes']


def get_ec2_instance(instanceid):
    return boto3.client('ec2').describe_instances(InstanceIds=[instanceid]).get('Reservations')[0]['Instances'][0]
    # return boto3.resource('ec2').Instance(instanceid)


def get_ec2_instance_price(instanceid):
    i = get_ec2_instance(instanceid)
    p = get_pricing(product='AmazonEC2', instanceType=i['InstanceType'])
    # returns current $ per hour price
    return float(list(list(p[0]['terms']['OnDemand'].values())[0]['priceDimensions'].values())[0]['pricePerUnit']['USD'])


def scan_custodian(filter=None):
    resources = glob.glob('out/**/resources.json', recursive=True)
    # pprint(resources)
    a = {"ebs": [], "ebs-snapshot": [], "rds": [], "ec2": [], 'other': [], 'all': []}
    c = []
    for f in resources:
        try:
            region = str(f).split("\\")[1]
        except Exception as e:
            region = str(f).split("/")[1]
        j = json.load(open(f))
        if j:
            for i in j:
                i.update({"item_region": region})
                if {'AvailabilityZone', 'CreateTime', 'Encrypted', 'Size', 'SnapshotId', 'State', 'VolumeId',
                    'VolumeType'}.issubset(i.keys()):  # ebs
                    i.update({'item_type': 'ebs'})
                    a['ebs'].append(i)
                elif {'Description', 'Encrypted', 'OwnerId', 'Progress', 'SnapshotId', 'StartTime', 'State', 'VolumeId',
                      'VolumeSize'}.issubset(i.keys()):  # snapshot
                    i.update({'item_type': 'ebs-snapshot'})
                    a['ebs-snapshot'].append(i)
                elif {'DBInstanceIdentifier', 'DBInstanceClass', 'Engine'}.issubset(i.keys()):  # rds
                    i.update({'item_type': 'rds'})
                    a['rds'].append(i)
                elif {'InstanceId', 'InstanceType', 'BlockDeviceMappings'}.issubset(i.keys()):  # ec2
                    i.update({'item_type': 'ec2'})
                    a['ec2'].append(i)
                else:
                    i.update({'item_type': 'other'})
                    a['other'].append(i)
                a['all'].append(i)
            c += j
    return a


def compile_ebs_costs():
    total_size = 0
    e = scan_custodian()
    if not e['ebs']:
        raise LookupError
    r = [p.get_price(x) for x in e['ebs']]
    return round(sum(r), 2)


def compile_ec2_costs():
    total_size = 0
    e = scan_custodian()
    volumes_by_region = {}
    for r in set([x['item_region'] for x in e['ec2']]):
        volumes_by_region.setdefault(r, [])
    for i in e['ec2']:
        for blockdevice in i['BlockDeviceMappings']:
            if 'Ebs' in blockdevice.keys():
                volumeid = blockdevice.get('Ebs').get('VolumeId')
                volumes_by_region[i['item_region']].append(volumeid)
    results = []
    for region, volumes in volumes_by_region.items():
        volchunks = [volumes[i:i + 99] for i in range(0, len(volumes), 99)]
        for chunk in volchunks:
            results += get_ebs_volumes(chunk, region)
    total_price = round(get_ebs_pricing('gp2') * sum([x['Size'] for x in results]), 2)
    # pprint("Total Price: {}".format(total_price))
    return total_price


def compile_rds_costs():
    e = scan_custodian()
    if not e['rds']:
        return None
    r = [p.get_price(x) for x in e['rds']]
    return round(sum(r) * 24 * 31, 2)


def compile_ebs_snapshot_costs():
    e = scan_custodian()['ebs-snapshot']
    return sum([p.get_price(x) for x in e])


if __name__ == '__main__':
    ec2 = boto3.client('ec2')
    p = Pricing()
    pass
